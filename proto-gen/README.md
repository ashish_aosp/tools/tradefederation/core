# DO NOT MODIFY, autogenerated with the following command from the root repo.

protoc --plugin=protoc-gen-grpc-java=prebuilts/tools/common/m2/repository/io/grpc/protoc-gen-grpc-java/1.0.3/protoc-gen-grpc-java-1.0.3-linux-x86_64.exe \
--grpc-java_out="tools/tradefederation/core/proto-gen/" \
--proto_path="tools/tradefederation/core/proto/monitoring/server/" \
"tools/tradefederation/core/proto/monitoring/server/tradefed_service.proto"
